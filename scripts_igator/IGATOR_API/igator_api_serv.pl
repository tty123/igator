#!/usr/bin/env perl
#===============================================================================
#
#         FILE: igator_api_serv.pl
#
#        USAGE: ./igator_api_serv.pl  
#
#  DESCRIPTION: api сервер для сервиса igator
#               сервер принимает http запрос и отдаёт в JSON результат
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 03.09.2019 10:05:26
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

use ClickHouse;
use JSON;
use AnyEvent::HTTPD;
use Data::Printer;
$| = 1;
my $ch;
#==== Подключаем ClickHouse ===============
#sub db_conn{
$ch = ClickHouse->new( # активируем коннект на кликхаус
	database => 'igator_db',
	host     => '172.17.0.188',
	user     => 'default',
	password => 'peretz',
	port     => 8123,
) or die "Error connecting clickhouse database";
#}
#&db_conn;
#----------------------------------------------------------

my $httpd = AnyEvent::HTTPD->new(port => 8460);

$httpd->reg_cb(
	request => sub {
		my ($httpd, $req) = @_;
		my $url = $req->url;
		my $mt = $req->method;
		my $hds = $req->headers;
		my %dt = $req->vars; # хэш значений формы
		my @dataset;
		if (($url eq '/igator') && ($mt eq 'POST')) {
			if ($dt{api} eq 'igator') {
				if ($dt{cmd} eq 'get_ip_data') {
					my $rows = $ch->select("SELECT time, node, ip, condition, data FROM igator_db.dataset  WHERE ip = '$dt{ip}' AND
				                                time BETWEEN '$dt{t_from}' AND '$dt{t_to}'") or die "error select database"; # запрос на получение данных
					#time IN (SELECT time FROM igator_db.dataset WHERE time >= '$dt{t_from}' AND time <= '$dt{t_to}'))"); # загружаем адреса конкретного узла
					@dataset = @$rows;
					p @dataset;
					if (@dataset) {
						my $json;
						if ($dataset[0][3] ne 'ok') {       # проверка наличия JSON-а в поле data
							$json = encode_json(\@dataset); # подготовили  к отправке
							#	foreach (@dataset){
							#	my $js_pr = join(',', @st_arr);
							#	my $json = "[$js_pr]";
							#

						}
						else {
							my $j_str = ();
							my @st_arr;
							foreach (@dataset) {
								my $js_data = pop @$_;
								p $js_data;
								$j_str = join('","', @$_);
								$j_str = "$j_str\",$js_data";
								$j_str = "[\"$j_str]";
								push(@st_arr, $j_str);
							}
							my $js_pr = join(',', @st_arr);
							$json = "[$js_pr]";
						}

						#if (@data){ my $json = encode_json(\@data); # подготовили  к отправке
						p $json;
						$req->respond({ content => [ 'application/json', $json ] });
						@dataset = ();
						print "command $dt{cmd} running \n";
					}
					else {$req->respond({ content => [ 'application/json', '{"error":"no result"}' ] })};
				} elsif ($dt{cmd} eq 'get_vermaxes_status'){
					#==== Возвращаем информацию по вермаксам, сигнал которых не соответствуют норме ===============
					$ch->do(" truncate table igator_db.temp_result;");
					$ch->do("

                            insert into igator_db.temp_result
                            select distinct norm_time, trouble, trouble_time, hours
                            from (
                                  select trouble_time,
                                         norm_time,
                                         dateDiff('hour', trouble_time, norm_time) as hours,
                                         trouble
                                  from (
                                           SELECT max(time)            trouble_time,
                                                  case
                                                      when
                                                              ((JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 2) < -65 or
                                                                JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 2) > 15) and
                                                               (JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 1) < -65 or
                                                                JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 1) > 15)) or
                                                              (JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 1) = -999 and
                                                               JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 2) = -999) or
                                                               (length(JSONExtractRaw(data, 'fnOpticalReceiverPower')) < 12 and (
                                                                     JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 1) < -65 or
                                                                     JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 1) > 15)
																) or
                                                              condition = 'not pinged'
                                                          then ip
                                                      else null end as trouble
                                           FROM igator_db.dataset
                                           WHERE time > now() - toIntervalHour(4)
                                             and ip in (select ip_address from igator_db.vermax_ip)
                                             and trouble is not null
                                           group by ip, trouble
                                           )
                                           left join (
                                      SELECT max(time)            norm_time,
                                             case
                                                 when
                                                         ((JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 2) > -65 and
                                                           JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 2) < 15 and
                                                           JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 1) = -999) or
                                                          (JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 1) > -65 and
                                                           JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 1) < 15 and
                                                           JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 2) = -999)) or
                                                         (JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 1) != -999 and
                                                          JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 2) != -999) or
                                                               (length(JSONExtractRaw(data, 'fnOpticalReceiverPower')) < 12 and (
                                                                     JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 1) > -65 and
                                                                     JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 1) < 15)
																)
                                                     then ip
                                                 else null end as notrouble
                                      FROM igator_db.dataset
                                      WHERE time > now() - toIntervalHour(48)
                                        and notrouble is not null
                                        and ip in (select ip_address from igator_db.vermax_ip)
                                      group by ip, notrouble
                                      ) on trouble = notrouble
                                  where trouble_time >= norm_time
                                     ) ;") or die "error create temp table";
					my @rows =$ch->select("
                        select norm_time, q/10, q1/10, trouble,trouble_time
                        from igator_db.temp_result
                                 inner join(
                            select JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 2) as q1,
                                   JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 1) as q,
                                   ip,
                                   time
                            from igator_db.dataset
                            where ip in (select trouble from igator_db.temp_result where hours <= -4)
                              and time >= (select min(trouble_time) from igator_db.temp_result where hours <= -4)
                              and time <= (select max(trouble_time) from igator_db.temp_result where hours <= -4)

                            ) on trouble = ip
                        where time = trouble_time
                          and ((q = -999 and q1 = -999) or hours <= -4) order by norm_time desc;
                                ") or die "error select database";
					$req->respond({ content => [ 'application/json', encode_json(\@rows) ] });
					print "command $dt{cmd} running \n";
				} elsif ($dt{cmd} eq 'get_vermax_all_status'){
                    #==== Возвращаем информацию по всем вермаксам ===============

                    my @rows =$ch->select("
                              select distinct JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 2) as q1,
                                              JSONExtractInt(JSONExtractRaw(data, 'fnOpticalReceiverPower'), 1) as q,
                                              d.ip,
                                              d.time
                              from igator_db.dataset d
                                       join (select ip, max(time) t
                                             from igator_db.dataset
                                             where ip in (select ip_address from igator_db.vermax_ip)
                                               and time > now() - toIntervalHour(5)
                                             group by ip) qq on qq.ip = d.ip and qq.t = d.time
                              where ip in (select ip_address from igator_db.vermax_ip)
                                and time > now() - toIntervalHour(5)
                                  ") or die "error select database";
                    $req->respond({ content => [ 'application/json', encode_json(\@rows) ] });
                    print "command $dt{cmd} running \n";
                }
                else {$req->respond({ content => [ 'application/json', '{"error":"cmd"}' ] })};
			}
			else {$req->respond({ content => [ 'application/json', '{"error":"api"}' ] })};
		}
		else {$req->respond({ content => [ 'application/json', '{"error":"url"}' ] })};
	},
);

#my $dbi_timer = AE::timer 0, 120, sub   # 2 минутный таймер для проверки и восстановления коннекта к базе
#{
#    if (defined (my $rows = $ch->select("show tables from igator_db")))
#    {
#        print "$rows clickhouse connect active.\n";
#    }
#    else
#    {
#        print "$rows clickhouse start connection.\n";
#        &db_conn;
#    }
#};
$httpd->run; # запуск
print "ok";