#!/usr/bin/env perl
#===============================================================================
#
#         FILE: fire_starter.pl
#
#        USAGE: ./fire_starter.pl  
#
#  DESCRIPTION: Скрипт для запуска node_requester.pl
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 09.07.2019 16:57:05
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use POSIX ":sys_wait_h";

$|=1;
#my $cmd='./node_requester.pl';

while (1){
system('./ip_loader.pl');
my @cmd=( 
	'./node_requester.pl vld_adm_umasheva_d2c_p1_n1',
	'./node_requester.pl vld_bestujeva_d20c_p1_n1',
	'./node_requester.pl vld_ilicheva_d32c_p1_n1',
        './node_requester.pl vld_krasn_znam_d86c_p1_n1',
        './node_requester.pl vld_nekrasovsk_d76c_p1_n1',
	'./node_requester.pl vld_nekrasovsk_d76c3_p1_n1',      
	'./node_requester.pl vld_nekrasovsk_d76c6_p1_n1',
	'./node_requester.pl vld_okatovaya_d10c_p1_n1',   
	'./node_requester.pl vld_okeanskiy_d108c_p1_n1',  
	'./node_requester.pl vld_okeanskiy_d125c3_p1_n1', 
	'./node_requester.pl vld_russkaya_d65c_p1_n1',    
	'./node_requester.pl vld_saxalinskaya_d34c_p1_n1',
	'./node_requester.pl vld_100-letia_d143c_p1_n1',  
	'./node_requester.pl arsen_lomonos_d42c_p1_n1',     
	'./node_requester.pl artem_kirova_d25c_p1_n1',      
	'./node_requester.pl fokino_ysatogo_d8c_p1_n1',    
	'./node_requester.pl naxodka_gorkogo_d21c_p1_n1',   
	'./node_requester.pl naxodka_leningrad_d17ac_p1_n1',
	'./node_requester.pl naxodka_postisheva_d27c_p1_n1',
	'./node_requester.pl trud_svetlaya_d66_p1_n1',      
	'./node_requester.pl ussur_chicherina_d153c_p1_n1', 
	'./node_requester.pl ussur_krasnoznam_d129c_p1_n1' 
	);

sub fork_child{
my ($child_process_code)=@_;
my $pid = fork;
die "fork not running:$!\n" if !defined $pid;
print "running fork!\n";
return $pid if $pid!=0;
$child_process_code->();
exit;
}

my $i=0;
my @pid;
foreach my $comm (@cmd){
$pid[$i]=fork_child(sub{
exec"$comm" or die 'node not starting: $!\n';
print "starting node_requester $pid[$i]\n";
});
$i++;
}
foreach (@pid){waitpid $_,0;}

print   "\n";
print   "\n";
print '              lll lll                      dd                     '."\n"; 
print '        aa aa lll lll  nn nnn   oooo       dd   eee   sss         '."\n"; 
print '       aa aaa lll lll  nnn  nn oo  oo  dddddd ee   e s            '."\n"; 
print '      aa  aaa lll lll  nn   nn oo  oo dd   dd eeeee   sss         '."\n";
print '       aaa aa lll lll  nn   nn  oooo   dddddd  eeeee     s        '."\n"; 
print '                                                      sss         '."\n";
print '                   lll lll               tt                dd  !!!'."\n"; 
print '       cccc  oooo  lll lll   eee    cccc tt      eee       dd  !!!'."\n"; 
print '     cc     oo  oo lll lll ee   e cc     tttt  ee   e  dddddd  !!!'."\n"; 
print '     cc     oo  oo lll lll eeeee  cc     tt    eeeee  dd   dd     '."\n"; 
print '      ccccc  oooo  lll lll  eeeee  ccccc  tttt  eeeee  dddddd  !!!'."\n"; 
print   "\n";
print   "\n";
}
