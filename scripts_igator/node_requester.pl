#!/usr/bin/env perl
#===============================================================================
#
#         FILE: node_requester.pl
#
#        USAGE: ./node_requester.pl  
#
#  DESCRIPTION: скрипт собирающий SNMP данные с коммутаторов узла. Для облегчения 
#		потоков убрана работа с DBI, используется только ClickHouse.
#               С базой данных Postgresql работает отдельный скрипт, который 
#               складывает адреса коммутаторов в ClickHouse.
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 2.1
#      CREATED: 17.05.2019 11:02:13
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

use threads;
use threads qw(yield);
use Thread::Queue;
use FindBin qw($Bin); # юзы для моего модуля
use lib $Bin;
use igator; 
use JSON;
use ClickHouse;
use Devel::Timer;
$|=1; 


#---------------------------------------------------------
#  Параметры узла. Имя узла передаётся в первом параметре к скрипту
#---------------------------------------------------------
my $node_ID = $ARGV[0]; #'vld_adm_umasheva_d2c_p1_n1'; # уникальный идентификатор узла

my @node_ipm = (); # массив ip-адресов принадлежащих узлу
#---------------------------------------------------------
# параметры SNMP
#---------------------------------------------------------
my $community = 'public';

#---------------------------------------------------------
my @snmp_response = ();
my @response = ();
#----------------------------------------------------------
#==== Подключаем ClickHouse ===============
my $ch = ClickHouse->new(                   # активируем коннект на кликхаус
	database => 'igator_db',
	host     => '172.17.0.188',
	user	 => 'default',
	password => 'peretz',
	port     => 8123,
	) or die "Error connecting clickhouse database" ;
#----------------------------------------------------------
#==== Процедура временной синхронизации ==================
my ($past_time_mark, $recent_time_mark, $delay_time) = igator -> time_mngmt();
$ch->do("INSERT INTO igator_db.node_state (time, node, state) VALUES (now(), '$node_ID', 'idle')");
print "past time mark ($past_time_mark), waiting $delay_time sec. for  new time mark ($recent_time_mark)\n";
sleep($delay_time);
$ch->do("INSERT INTO igator_db.node_state (time, node, state) VALUES (now(), '$node_ID', 'starting')");
print "recent time mark ($recent_time_mark), running snmp data collecting...\n";
#---------------------------------------------------------

my $rows = $ch->select("SELECT id, ips, up_date FROM igator_db.iptbl FINAL where id = '$node_ID'"); # загружаем адреса конкретного узла
my $str = $rows->[0]->[1];
@node_ipm = split(' ',$str);

#-------------- проверяем доступность адресов, отделяем молчащих  -------------------------------------
my ($ref_ipm_pinged, $ref_ipm_notpinged) = igator -> ping_ipm (\@node_ipm);
my @node_ipm_pinged = @$ref_ipm_pinged;
my @node_ipm_notpinged = @$ref_ipm_notpinged;

#----- забираем модель устройства для каждого адреса  -----

# получаем ссылку на хэш с vendor_oid и массив подвисших моделей, которые пингуются, но не отдают по SNMP 
my ($vendor_ipm_snmp, $ip_snmp_undef) = igator -> get_vendor_id (@node_ipm_pinged);
my $number = scalar @node_ipm; 
print "all node ip numbers = $number \n";
my $size = scalar keys %{$vendor_ipm_snmp};
print "vendor_oid_ip_numbers = $size\n"; 

#----- готовим финальный массив адресов, пропустив через список accepted vendor ID -----
my ($accepted_vendor_ip, $blocked_vendor_ip, $new_vendor_ip) = igator -> sort_vendor_id ($vendor_ipm_snmp);

#------ отрабатываем многопоточный сбор данных ------
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
my $t = Devel::Timer -> new();
$t -> mark ('start');
my $q = Thread::Queue -> new();
my @acc_ipm = keys %$accepted_vendor_ip;
#splice (@acc_ipm,40);
my @thr;
	$thr[0] = threads->new(\&queue_build_hash);  # поток обработки очереди
	my $threads =50; # задаём количество тредов (default 30)
	my $num_thr = $#acc_ipm+2; # количество snmp потоков 
	while (@acc_ipm){
		
                 for (my $i=1; $i <= $threads; $i++){  # организовываем $threads потоков по каждому ip
			my $ip = pop (@acc_ipm);
				unless (defined $ip) {$thr[$i] = threads -> new(sub{return;}); next;} # костыль
			  $thr[$i] = threads->new ({'stack_size' => 128*4096}, sub {
	my $res_hsh = igator -> get_ip_data ($ip, $accepted_vendor_ip);
#print "\$res_hsh = $res_hsh\n";
		if (defined($res_hsh)){$q -> enqueue($res_hsh);} # кидаем данные в очередь
#print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n";
			});

		}
			for (my $i=1; $i <= $threads; $i++){
		        $thr[$i]->join();
			}
	$num_thr = $num_thr - $threads;
	print "--------- \$num_thr =  $num_thr ---- \$threads = $threads -----\@acc_ipm = $#acc_ipm --------------\n";
	if ($num_thr<$threads){$threads=$num_thr}
	}
print "cycle ended!\n";
$q -> enqueue('end');# маркируем конец очереди
print "enqueue end\n";
$thr[0] -> join(); # дожидаемся обработки очереди
@thr = ();
threads->yield();
print "queue joined!!!\n";
	my $ref_data = $q -> dequeue();
	my @dat = @$ref_data; 

#print Dumper @dat;
#---- формируем и отправляем полученные данные в clickhouse ----
#++++ данные по ping-ам ip адресов 
my @str;
my @arrstr;
	foreach (@node_ipm_notpinged) {
		@str=($recent_time_mark, $node_ID, $_, 'not pinged');
		push(@arrstr, [@str]);
		}
#---- 
#++++ состояние по зависшим, исключённым и новым устройствам 

	foreach ( @$ip_snmp_undef) {
		@str = ( $recent_time_mark, $node_ID, $_, 'not snmp'); 
		push(@arrstr, [@str]);
		}
	foreach (keys (%$blocked_vendor_ip)) {
		@str = ( $recent_time_mark, $node_ID, $_, 'excluded');
		push(@arrstr, [@str]);
		}
	foreach (keys (%$new_vendor_ip)) {
		@str = ($recent_time_mark, $node_ID, $_, 'new model');
		push(@arrstr, [@str]);
		}
	$ch->do("INSERT INTO igator_db.dataset (time, node, ip, condition) VALUES", @arrstr ); 
#+++ snmp данные по ip
undef @arrstr;
undef @str;
	foreach (@dat) {
		my %ip_hash=%$_;
		my @ip = keys %ip_hash;
		my $ip_json = $_->{$ip[0]};
		@str = ( $recent_time_mark, $node_ID , $ip[0], $ip_json, 'ok');  # @str содержит ip и  json для инсерта 
		push(@arrstr, [@str]);
		}
	$ch->do("INSERT INTO igator_db.dataset (time, node, ip, data, condition) VALUES", @arrstr);
	$ch->do("INSERT INTO igator_db.node_state (time, node, state) VALUES (now(),'$node_ID', 'ending')"); # нода закончила работать
	$ch->disconnect;
undef $recent_time_mark;
undef @dat;
#---- 

$t -> mark ('end ');
$t -> report();
print "$#acc_ipm num of ip adresses\n";


#---------------- очередь --------------------
sub queue_build_hash {
	my $q_data;
	my @data;
	my %temp_main;
	my %temp;
	while ($q_data ne 'end'){
		 $q_data = $q -> dequeue();
		if ($q_data ne 'end'){ my @hip; @hip =  keys (%$q_data);
					my $json = encode_json ($q_data->{$hip[0]});
					my %new_hsh = ($hip[0] => $json);
					push(@data, \%new_hsh);} # здесь формируем json данных по ip
	}
					undef $q_data;
		$q -> enqueue(\@data); # кидаем данные в очередь
}
#-----------------------------------------------
#---         this is the end                 ---
#-----------------------------------------------
