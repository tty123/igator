#
#===============================================================================
#
#         FILE: igator.pm
#
#  DESCRIPTION: модуль для работы с OID-ами разных моделей коммутаторов
#               сети Vladlink. 
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 23.04.2019 09:42:40
#     REVISION: ---
#===============================================================================

package igator;
use strict;
use warnings;
use AnyEvent;
#use AnyEvent::SNMP;
use Net::SNMP;
#use Net::IP::Match::XS;
#use Data::Printer;
#use Data::Dumper; 
use AsyncPing;
use JSON;


our $VERSION = '1.00';


sub new {
 my $class = shift;
 my $self = bless{}, $class;
 return $self;
 }


my @oid_sname_objid = ('1.3.6.1.2.1.1.5', '1.3.6.1.2.1.1.2'); # запрос имени устройства, запрос VendorID

my $sysObjectID = '1.3.6.1.2.1.1.2.0'; # вендор ID

my @ignored_vendor = ('1.3.6.1.4.1.17409.1.7', '1.3.6.1.4.1.17409.1.10'); # массив игнорируемых устройств

my $community = 'public';

my %dlink1=(                           # D-Link DES-2108
	'sysDescr'      => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'   => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'     => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'       => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus' => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'  => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts' => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts' => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors' => '1.3.6.1.2.1.2.2.1,13', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts' => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifOutNUcastPkts' => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifOutErrors' => '1.3.6.1.2.1.2.2.1,13', # массив значений по каждому сетевому интерфейсу
);

my %dlink2=(                           # DES-3200-28P/C1  D-Link-3200-52/C1
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Power, Fan, Temp --
	'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
	'swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
	'swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
	'swFanUnitIndex'              => '1.3.6.1.4.1.171.12.11.1.7.1.1', # выдаёт 2 строки для 2-х вентиляторов swFanUnitIndex.1.7 = 1, swFanUnitIndex.1.8 = 1
	'swFanID'                     => '1.3.6.1.4.1.171.12.11.1.7.1.2', # выдаёт 2 строки ID каждого вентилятора swFanID.1.7 = 7, swFanID.1.8 = 8
	'swFanStatus'                 => '1.3.6.1.4.1.171.12.11.1.7.1.3', # выдаёт 2 строки состояние каждого вентилятора {other(0),working(1),fail(2),speed-0(3),speed-low(4),speed-middle(5),speed-high(6)}
	'swTemperatureCurrent'        => '1.3.6.1.4.1.171.12.11.1.8.1.2', # информация с температурного датчика(ов) в градусах
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
	#'agentFirmwareType'           => '1.3.6.1.4.1.171.12.1.1.13', # тип прошивки

);

my %dlink3=(                          # DES-3200-28
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Power, Fan, Temp --
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
);

my %dlink4=(                          # DES-3526
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки (swDevInfoSystemUpTime .1.3.6.1.4.1.171.11.64.1.2.1.1.1.0)
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	'swDevInfoTotalNumOfPort'     => '1.3.6.1.4.1.171.11.64.1.2.1.1.2', # количество портов в свиче
	'swDevInfoNumOfPortInUse'     => '1.3.6.1.4.1.171.11.64.1.2.1.1.3', # количество задействованных портов в устройстве
	'swDevInfoPowerUnitIndex'     => '1.3.6.1.4.1.171.11.64.1.2.1.1.8.1.1', # индикация ID блока питания в системе (наличие питальников в системе)
	'swDevInfoPowerID'	      => '1.3.6.1.4.1.171.11.64.1.2.1.1.8.1.2', # показывает ID источников питания
	'swDevInfoPowerStatus'        => '1.3.6.1.4.1.171.11.64.1.2.1.1.8.1.3', # статус блока питания {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
	#-- Vlan --
	'swL2VlanIndex'               => '1.3.6.1.4.1.171.11.64.1.2.3.1.1.1', # The VLAN-ID or other identifier refering to this VLAN.
	'swL2VlanName'                => '1.3.6.1.4.1.171.11.64.1.2.3.1.1.2', # An administratively assigned string, which may be used to identify the VLAN.
	'swL2VlanAdvertiseState'      => '1.3.6.1.4.1.171.11.64.1.2.3.1.1.3', # This object indicates the advertise status of this vlan entry.
	#-- swL2PortInfoEntry--
	'swL2PortInfoLinkStatus'      => '1.3.6.1.4.1.171.11.64.1.2.4.1.1.4', # This object indicates the port link status.
	'swL2PortInfoNwayStatus'      => '1.3.6.1.4.1.171.11.64.1.2.4.1.1.5', # Iindicates the port speed and duplex mode.{other(1),auto(2),half-10Mbps(3),full-10Mbps(4),half-100Mbps(5),full-100Mbps(6),half-1Gigabps(7),full-1Gigabps(8)}
	#-- swL2LimitedMulticastMgmt --
	'swL2MulticastRangeFromIp'    => '1.3.6.1.4.1.171.11.64.1.2.5.1.1.2', # This object specifies begin IP address for this range.
	'swL2MulticastRangeToIp'      => '1.3.6.1.4.1.171.11.64.1.2.5.1.1.3', # This object specifies end IP address for this range.
	#-- swL2IGMPMgmt swL2IGMPInfoTable--
	'swL2IGMPCtrlVid'             => '1.3.6.1.4.1.171.11.64.1.2.10.3.1.1', # This object indicates the IGMP control entry's VLAN id. If VLAN is disabled, the Vid is always 0
	'swL2IGMPVid'                 => '1.3.6.1.4.1.171.11.64.1.2.10.5.1.1', # This object indicates the Vid of individual IGMP table entry.
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Power, Fan, Temp --
	'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
	'swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
	'swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
);

my %dlink5=(                           # DGS-1210-28/ME/B1 (enterprise.171.10.76.28.2)
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics -- DGS-1210-28/me/B1 only
	'cableDiagLinkStatus'         => '1.3.6.1.4.1.171.10.76.28.2.35.1.1.3', # (.*) состояние линка-соединения {linkdown(0),linkup(1),other(2)} 	
	'cableDiagPair1Status'        => '1.3.6.1.4.1.171.10.76.28.2.35.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)}	
	'cableDiagPair2Status'        => '1.3.6.1.4.1.171.10.76.28.2.35.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)}	
	'cableDiagPair3Status'        => '1.3.6.1.4.1.171.10.76.28.2.35.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)} 	
	'cableDiagPair4Status'        => '1.3.6.1.4.1.171.10.76.28.2.35.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)}	
	'cableDiagPair1Length'        => '1.3.6.1.4.1.171.10.76.28.2.35.1.1.8', # (.*) Cable Diagnostics pair 1 fault distance.
	'cableDiagPair2Length'        => '1.3.6.1.4.1.171.10.76.28.2.35.1.1.9', # (.*) Cable Diagnostics pair 2 fault distance. 
	'cableDiagPair3Length'        => '1.3.6.1.4.1.171.10.76.28.2.35.1.1.10', # (.*) Cable Diagnostics pair 3 fault distance.
	'cableDiagPair4Length'        => '1.3.6.1.4.1.171.10.76.28.2.35.1.1.11', # (.*) Cable Diagnostics pair 4 fault distance.
	#-- CPU, Port, RAM utilization --
	'sysSerialNumber'	      => '1.3.6.1.4.1.171.10.76.28.2.1.30', # серийный номер устройства
	'sysFirmwareVersion'          => '1.3.6.1.4.1.171.10.76.28.2.1.3', # тип прошивки
	#-- Power 220 - UPS
	'powerStatus'                 => '1.3.6.1.4.1.171.10.76.28.2.109.1.1.2', # состояние линии питания (0-220в. 1-UPS){other(1), lowVoltage(2), overCurrent(3), working(4), fail(5), connect(6), disconnect(7)}
	
);

my %dlink6=(                           # DGS-1210-52/ME/A1 (enterprise.171.10.76.29.1)
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics -- DGS-1210-28/me/B1 only
	'cableDiagLinkStatus'         => '1.3.6.1.4.1.171.10.76.29.1.35.1.1.3', # (.*) состояние линка-соединения {linkdown(0),linkup(1),other(2)} 	
	'cableDiagPair1Status'        => '1.3.6.1.4.1.171.10.76.29.1.35.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)}	
	'cableDiagPair2Status'        => '1.3.6.1.4.1.171.10.76.29.1.35.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)}	
	'cableDiagPair3Status'        => '1.3.6.1.4.1.171.10.76.29.1.35.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)} 	
	'cableDiagPair4Status'        => '1.3.6.1.4.1.171.10.76.29.1.35.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)}	
	'cableDiagPair1Length'        => '1.3.6.1.4.1.171.10.76.29.1.35.1.1.8', # (.*) Cable Diagnostics pair 1 fault distance.
	'cableDiagPair2Length'        => '1.3.6.1.4.1.171.10.76.29.1.35.1.1.9', # (.*) Cable Diagnostics pair 2 fault distance. 
	'cableDiagPair3Length'        => '1.3.6.1.4.1.171.10.76.29.1.35.1.1.10', # (.*) Cable Diagnostics pair 3 fault distance.
	'cableDiagPair4Length'        => '1.3.6.1.4.1.171.10.76.29.1.35.1.1.11', # (.*) Cable Diagnostics pair 4 fault distance.
	#-- CPU, Port, RAM utilization --
	'sysSerialNumber'	      => '1.3.6.1.4.1.171.10.76.29.1.1.30', # серийный номер устройства
	'sysFirmwareVersion'          => '1.3.6.1.4.1.171.10.76.29.1.1.3', # тип прошивки

);

my %dlink7=(                           # DGS-1210-52/ME/B1 (enterprise.171.10.76.29.2)
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics -- DGS-1210-28/me/B1 only
	'cableDiagLinkStatus'         => '1.3.6.1.4.1.171.10.76.29.2.35.1.1.3', # (.*) состояние линка-соединения {linkdown(0),linkup(1),other(2)} 	
	'cableDiagPair1Status'        => '1.3.6.1.4.1.171.10.76.29.2.35.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)}	
	'cableDiagPair2Status'        => '1.3.6.1.4.1.171.10.76.29.2.35.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)}	
	'cableDiagPair3Status'        => '1.3.6.1.4.1.171.10.76.29.2.35.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)} 	
	'cableDiagPair4Status'        => '1.3.6.1.4.1.171.10.76.29.2.35.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)}	
	'cableDiagPair1Length'        => '1.3.6.1.4.1.171.10.76.29.2.35.1.1.8', # (.*) Cable Diagnostics pair 1 fault distance.
	'cableDiagPair2Length'        => '1.3.6.1.4.1.171.10.76.29.2.35.1.1.9', # (.*) Cable Diagnostics pair 2 fault distance. 
	'cableDiagPair3Length'        => '1.3.6.1.4.1.171.10.76.29.2.35.1.1.10', # (.*) Cable Diagnostics pair 3 fault distance.
	'cableDiagPair4Length'        => '1.3.6.1.4.1.171.10.76.29.2.35.1.1.11', # (.*) Cable Diagnostics pair 4 fault distance.
	#-- CPU, Port, RAM utilization --
	'sysSerialNumber'	      => '1.3.6.1.4.1.171.10.76.29.2.1.30', # серийный номер устройства
	'sysFirmwareVersion'          => '1.3.6.1.4.1.171.10.76.29.2.1.3', # тип прошивки
	#-- Fan -- DGS-1210-52/ME/B1
	'sysSmartFanStatus'           => '1.3.6.1.4.1.171.10.76.29.2.1.33.1', # The Fan status for this device {failure(0),normal(1)}
	'sysTemperature'              => '1.3.6.1.4.1.171.10.76.29.2.1.33.2', # The Temperature of this device.
	#-- Power 220 - UPS
	'powerStatus'                 => '1.3.6.1.4.1.171.10.76.29.2.109.1.1.2', # состояние линии питания (0-220в. 1-UPS){other(1), lowVoltage(2), overCurrent(3), working(4), fail(5), connect(6), disconnect(7)}
);

my %dlink8=(                           # DGS-3120-24SC Gigabit Ethernet Switch (enterprise.171.10.117.1.3) 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Port utilization --
	'swDevInfoTotalNumOfPort'     => '1.3.6.1.4.1.171.11.117.1.3.2.1.1.1', # количество портов в свиче
	'swDevInfoNumOfPortInUse'     => '1.3.6.1.4.1.171.11.117.1.3.2.1.1.2', # количество подключённых портов
	#-- Power, Fan, Temp --
	'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
	'swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
	'swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
	'swFanUnitIndex'              => '1.3.6.1.4.1.171.12.11.1.7.1.1', # выдаёт 2 строки для 2-х вентиляторов swFanUnitIndex.1.7 = 1, swFanUnitIndex.1.8 = 1
	'swFanID'                     => '1.3.6.1.4.1.171.12.11.1.7.1.2', # выдаёт 2 строки ID каждого вентилятора swFanID.1.7 = 7, swFanID.1.8 = 8
	'swFanStatus'                 => '1.3.6.1.4.1.171.12.11.1.7.1.3', # выдаёт 2 строки состояние каждого вентилятора {other(0),working(1),fail(2),speed-0(3),speed-low(4),speed-middle(5),speed-high(6)}
	'swTemperatureCurrent'        => '1.3.6.1.4.1.171.12.11.1.8.1.2', # информация с температурного датчика(ов) в градусах
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
	'agentFirmwareType'           => '1.3.6.1.4.1.171.12.1.1.13', # тип прошивки

);

my %dlink9=(                           # DGS-3120-24SC Gigabit Ethernet Switch (enterprise.171.10.117.1.3) 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Port utilization --
	'swDevInfoTotalNumOfPort'     => '1.3.6.1.4.1.171.11.117.1.1.2.1.1.1', # количество портов в свиче
	'swDevInfoNumOfPortInUse'     => '1.3.6.1.4.1.171.11.117.1.1.2.1.1.2', # количество подключённых портов
	#-- Power, Fan, Temp --
	'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
	'swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
	'swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
	'swFanUnitIndex'              => '1.3.6.1.4.1.171.12.11.1.7.1.1', # выдаёт 2 строки для 2-х вентиляторов swFanUnitIndex.1.7 = 1, swFanUnitIndex.1.8 = 1
	'swFanID'                     => '1.3.6.1.4.1.171.12.11.1.7.1.2', # выдаёт 2 строки ID каждого вентилятора swFanID.1.7 = 7, swFanID.1.8 = 8
	'swFanStatus'                 => '1.3.6.1.4.1.171.12.11.1.7.1.3', # выдаёт 2 строки состояние каждого вентилятора {other(0),working(1),fail(2),speed-0(3),speed-low(4),speed-middle(5),speed-high(6)}
	'swTemperatureCurrent'        => '1.3.6.1.4.1.171.12.11.1.8.1.2', # информация с температурного датчика(ов) в градусах
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
	'agentFirmwareType'           => '1.3.6.1.4.1.171.12.1.1.13', # тип прошивки

);

my %dlink10=(                           # DGS-3420-28SC Gigabit Ethernet Switch (enterprise.171.10.119.2) 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Port utilization --
	'swDevInfoTotalNumOfPort'     => '1.3.6.1.4.1.171.11.119.2.2.1.1.1', # количество портов в свиче
	'swDevInfoNumOfPortInUse'     => '1.3.6.1.4.1.171.11.119.2.2.1.1.2', # количество подключённых портов
	#-- Power, Fan, Temp --
	'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
	'swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
	'swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
	'swFanUnitIndex'              => '1.3.6.1.4.1.171.12.11.1.7.1.1', # выдаёт 2 строки для 2-х вентиляторов swFanUnitIndex.1.7 = 1, swFanUnitIndex.1.8 = 1
	'swFanID'                     => '1.3.6.1.4.1.171.12.11.1.7.1.2', # выдаёт 2 строки ID каждого вентилятора swFanID.1.7 = 7, swFanID.1.8 = 8
	'swFanStatus'                 => '1.3.6.1.4.1.171.12.11.1.7.1.3', # выдаёт 2 строки состояние каждого вентилятора {other(0),working(1),fail(2),speed-0(3),speed-low(4),speed-middle(5),speed-high(6)}
	'swTemperatureCurrent'        => '1.3.6.1.4.1.171.12.11.1.8.1.2', # информация с температурного датчика(ов) в градусах
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства

);

my %dlink11=(                          # DGS-3627G Gigabit Ethernet Switch (enterprise.171.10.70.8)
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	#'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Port utilization --
	'swDevInfoTotalNumOfPort'     => '1.3.6.1.4.1.171.11.70.8.2.1.1.1', # количество портов в свиче
	'swDevInfoNumOfPortInUse'     => '1.3.6.1.4.1.171.11.70.8.2.1.1.2', # количество подключённых портов
	'swDevInfoFirmwareVersion'    => '1.3.6.1.4.1.171.11.70.8.2.1.1.5', # Boot firmware Version.
	#-- Power, Fan, Temp --
	'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
	'swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
	'swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
	'swFanUnitIndex'              => '1.3.6.1.4.1.171.12.11.1.7.1.1', # выдаёт 2 строки для 2-х вентиляторов swFanUnitIndex.1.7 = 1, swFanUnitIndex.1.8 = 1
	'swFanID'                     => '1.3.6.1.4.1.171.12.11.1.7.1.2', # выдаёт 2 строки ID каждого вентилятора swFanID.1.7 = 7, swFanID.1.8 = 8
	'swFanStatus'                 => '1.3.6.1.4.1.171.12.11.1.7.1.3', # выдаёт 2 строки состояние каждого вентилятора {other(0),working(1),fail(2),speed-0(3),speed-low(4),speed-middle(5),speed-high(6)}
	#'swTemperatureCurrent'        => '1.3.6.1.4.1.171.12.11.1.8.1.2', # информация с температурного датчика(ов) в градусах
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
	#'agentFirmwareType'           => '1.3.6.1.4.1.171.12.1.1.13', # тип прошивки

);

my %dlink12=(                          # D-Link DES-3026 Fast Ethernet Switch (enterprise.171.10.63.3) 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	#'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	#'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'swL2CPUutilizationIn5sec'    => '1.3.6.1.4.1.171.11.63.3.2.1.3.1', # загрузка CPU в % за 5сек интервал
	'swL2CPUutilizationIn1min'    => '1.3.6.1.4.1.171.11.63.3.2.1.3.2', # загрузка CPU в % за 1мин  интервал
	'swL2CPUutilizationIn5min'    => '1.3.6.1.4.1.171.11.63.3.2.1.3.3', # загрузка CPU в % за 5мин интервал
	'swL2PortInfoLinkStatus'      => '1.3.6.1.4.1.171.11.63.3.2.2.1.1.4', # (.*) This object indicates the port link status. 
	'swL2PortInfoNwayStatus'      => '1.3.6.1.4.1.171.11.63.3.2.2.1.1.5', # (.*) Скорость и режим работы порта {auto(1),full-10Mbps-8023x(2),full-10Mbps-none(3),half-10Mbps-backp(4),half-10Mbps-none(5),full-100Mbps-8023x(6),full-100Mbps-none(7),half-100Mbps-backp(8),half-100Mbps-none(9),full-1Gigabps-8023x(10),full-1Gigabps-none(11),half-1Gigabps-backp(12),half-1Gigabps-none(13)}
	'swL2IGMPCurrentState'        => '1.3.6.1.4.1.171.11.63.3.2.7.3.1.10', # (.*) This object indicates the current IGMP query state.
	'swL2LoopDetectPortLoopStatus'=> '1.3.6.1.4.1.171.11.63.3.2.18.2.1.1.4', # (.*) индикатор состояния портов (ошибок, петель)   {normal(1),loop(2),error(3)}

	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	#'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Port utilization --
	#'swDevInfoTotalNumOfPort'     => '1.3.6.1.4.1.171.11.70.8.2.1.1.1', # количество портов в свиче
	#'swDevInfoNumOfPortInUse'     => '1.3.6.1.4.1.171.11.70.8.2.1.1.2', # количество подключённых портов
	#'swDevInfoFirmwareVersion'    => '1.3.6.1.4.1.171.11.70.8.2.1.1.5', # Boot firmware Version.
	#-- Power, Fan, Temp --
	#'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
	#swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
	#swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
	#swFanUnitIndex'              => '1.3.6.1.4.1.171.12.11.1.7.1.1', # выдаёт 2 строки для 2-х вентиляторов swFanUnitIndex.1.7 = 1, swFanUnitIndex.1.8 = 1
	#'swFanID'                     => '1.3.6.1.4.1.171.12.11.1.7.1.2', # выдаёт 2 строки ID каждого вентилятора swFanID.1.7 = 7, swFanID.1.8 = 8
	#'swFanStatus'                 => '1.3.6.1.4.1.171.12.11.1.7.1.3', # выдаёт 2 строки состояние каждого вентилятора {other(0),working(1),fail(2),speed-0(3),speed-low(4),speed-middle(5),speed-high(6)}
	#'swTemperatureCurrent'        => '1.3.6.1.4.1.171.12.11.1.8.1.2', # информация с температурного датчика(ов) в градусах
	#'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
	#'agentFirmwareType'           => '1.3.6.1.4.1.171.12.1.1.13', # тип прошивки

);

my %dlink13=(                           # D-Link DES-3028 Fast Ethernet Switch (enterprise.171.10.63.6) 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Power, Fan, Temp --
#	'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
#	'swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
#	'swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
#	'swFanUnitIndex'              => '1.3.6.1.4.1.171.12.11.1.7.1.1', # выдаёт 2 строки для 2-х вентиляторов swFanUnitIndex.1.7 = 1, swFanUnitIndex.1.8 = 1
#	'swFanID'                     => '1.3.6.1.4.1.171.12.11.1.7.1.2', # выдаёт 2 строки ID каждого вентилятора swFanID.1.7 = 7, swFanID.1.8 = 8
#	'swFanStatus'                 => '1.3.6.1.4.1.171.12.11.1.7.1.3', # выдаёт 2 строки состояние каждого вентилятора {other(0),working(1),fail(2),speed-0(3),speed-low(4),speed-middle(5),speed-high(6)}
#	'swTemperatureCurrent'        => '1.3.6.1.4.1.171.12.11.1.8.1.2', # информация с температурного датчика(ов) в градусах
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
#	'agentFirmwareType'           => '1.3.6.1.4.1.171.12.1.1.13', # тип прошивки
	#--Link status--
	'swL2PortInfoLinkStatus' => '1.3.6.1.4.1.171.11.63.6.2.2.1.1.4', # (.*)  This object indicates the port link status. {other(1),link-pass(2),link-fail(3)}
	'swL2PortInfoNwayStatus' => '1.3.6.1.4.1.171.11.63.6.2.2.1.1.5', # (.*) Режим работы портов {auto(1),half-10Mbps(2),full-10Mbps(3),half-100Mbps(4),full-100Mbps(5),half-1Gigabps(6),full-1Gigabps(7)}
	'swL2IGMPCurrentState'   => '1.3.6.1.4.1.171.11.63.6.2.7.3.1.10', # (.*)  This object indicates the current IGMP query state. {other(1),querier(2),non-querier(3)}
	'swL2IGMPInfoQueryCount' => '1.3.6.1.4.1.171.11.63.6.2.7.4.1.2', # (.*) Количество IGMP пакетов принятых по каждому V-лану
	'swL2LoopDetectPortLoopStatus'=> '1.3.6.1.4.1.171.11.63.6.2.21.2.1.1.4', # (.*) Детектор петель на порту {normal(1),loop(2),error(3)}
);

my %dlink14=(                           #  D-Link DES-3028G Fast Ethernet Switch (enterprise.171.10.63.11) 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Power, Fan, Temp --
#	'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
#	'swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
#	'swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
#	'swFanUnitIndex'              => '1.3.6.1.4.1.171.12.11.1.7.1.1', # выдаёт 2 строки для 2-х вентиляторов swFanUnitIndex.1.7 = 1, swFanUnitIndex.1.8 = 1
#	'swFanID'                     => '1.3.6.1.4.1.171.12.11.1.7.1.2', # выдаёт 2 строки ID каждого вентилятора swFanID.1.7 = 7, swFanID.1.8 = 8
#	'swFanStatus'                 => '1.3.6.1.4.1.171.12.11.1.7.1.3', # выдаёт 2 строки состояние каждого вентилятора {other(0),working(1),fail(2),speed-0(3),speed-low(4),speed-middle(5),speed-high(6)}
#	'swTemperatureCurrent'        => '1.3.6.1.4.1.171.12.11.1.8.1.2', # информация с температурного датчика(ов) в градусах
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
#	'agentFirmwareType'           => '1.3.6.1.4.1.171.12.1.1.13', # тип прошивки
	#--Link status--
	'swL2PortInfoLinkStatus' => '1.3.6.1.4.1.171.11.63.11.2.2.1.1.4', # (.*)  This object indicates the port link status. {other(1),link-pass(2),link-fail(3)}
	'swL2PortInfoNwayStatus' => '1.3.6.1.4.1.171.11.63.11.2.2.1.1.5', # (.*) Режим работы портов {auto(1),half-10Mbps(2),full-10Mbps(3),half-100Mbps(4),full-100Mbps(5),half-1Gigabps(6),full-1Gigabps(7)}
	'swL2IGMPCurrentState'   => '1.3.6.1.4.1.171.11.63.11.2.7.3.1.10', # (.*)  This object indicates the current IGMP query state. {other(1),querier(2),non-querier(3)}
	'swL2IGMPInfoQueryCount' => '1.3.6.1.4.1.171.11.63.11.2.7.4.1.2', # (.*) Количество IGMP пакетов принятых по каждому V-лану
	'swL2LoopDetectPortLoopStatus'=> '1.3.6.1.4.1.171.11.63.11.2.21.2.1.1.4', # (.*) Детектор петель на порту {normal(1),loop(2),error(3)}
);

my %dlink15=(                           # D-Link DES-3028P Fast Ethernet Switch (enterprise.171.10.63.7) 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Power, Fan, Temp --
#	'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
#	'swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
#	'swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
#	'swFanUnitIndex'              => '1.3.6.1.4.1.171.12.11.1.7.1.1', # выдаёт 2 строки для 2-х вентиляторов swFanUnitIndex.1.7 = 1, swFanUnitIndex.1.8 = 1
#	'swFanID'                     => '1.3.6.1.4.1.171.12.11.1.7.1.2', # выдаёт 2 строки ID каждого вентилятора swFanID.1.7 = 7, swFanID.1.8 = 8
#	'swFanStatus'                 => '1.3.6.1.4.1.171.12.11.1.7.1.3', # выдаёт 2 строки состояние каждого вентилятора {other(0),working(1),fail(2),speed-0(3),speed-low(4),speed-middle(5),speed-high(6)}
#	'swTemperatureCurrent'        => '1.3.6.1.4.1.171.12.11.1.8.1.2', # информация с температурного датчика(ов) в градусах
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
#	'agentFirmwareType'           => '1.3.6.1.4.1.171.12.1.1.13', # тип прошивки
	#--Link status--
	'swL2PortInfoLinkStatus' => '1.3.6.1.4.1.171.11.63.7.2.2.1.1.4', # (.*)  This object indicates the port link status. {other(1),link-pass(2),link-fail(3)}
	'swL2PortInfoNwayStatus' => '1.3.6.1.4.1.171.11.63.7.2.2.1.1.5', # (.*) Режим работы портов {auto(1),half-10Mbps(2),full-10Mbps(3),half-100Mbps(4),full-100Mbps(5),half-1Gigabps(6),full-1Gigabps(7)}
	'swL2IGMPCurrentState'   => '1.3.6.1.4.1.171.11.63.7.2.7.3.1.10', # (.*)  This object indicates the current IGMP query state. {other(1),querier(2),non-querier(3)}
	'swL2IGMPInfoQueryCount' => '1.3.6.1.4.1.171.11.63.7.2.7.4.1.2', # (.*) Количество IGMP пакетов принятых по каждому V-лану
	'swL2LoopDetectPortLoopStatus'=> '1.3.6.1.4.1.171.11.63.7.2.21.2.1.1.4', # (.*) Детектор петель на порту {normal(1),loop(2),error(3)}
);

my %dlink16=(                           # D-Link DES-3200-28 Fast Ethernet Switch (enterprise.171.10.113.1.3) 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Power, Fan, Temp --
#	'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
#	'swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
#	'swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
#	'swFanUnitIndex'              => '1.3.6.1.4.1.171.12.11.1.7.1.1', # выдаёт 2 строки для 2-х вентиляторов swFanUnitIndex.1.7 = 1, swFanUnitIndex.1.8 = 1
#	'swFanID'                     => '1.3.6.1.4.1.171.12.11.1.7.1.2', # выдаёт 2 строки ID каждого вентилятора swFanID.1.7 = 7, swFanID.1.8 = 8
#	'swFanStatus'                 => '1.3.6.1.4.1.171.12.11.1.7.1.3', # выдаёт 2 строки состояние каждого вентилятора {other(0),working(1),fail(2),speed-0(3),speed-low(4),speed-middle(5),speed-high(6)}
#	'swTemperatureCurrent'        => '1.3.6.1.4.1.171.12.11.1.8.1.2', # информация с температурного датчика(ов) в градусах
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
#	'agentFirmwareType'           => '1.3.6.1.4.1.171.12.1.1.13', # тип прошивки
	#--Link status--
	'swL2PortInfoLinkStatus' => '1.3.6.1.4.1.171.11.113.1.3.2.2.1.1.4', # (.*)  This object indicates the port link status. {other(1),link-pass(2),link-fail(3)}
	'swL2PortInfoNwayStatus' => '1.3.6.1.4.1.171.11.113.1.3.2.2.1.1.5', # (.*)Режим работы портов {auto(1),half-10Mbps(2),full-10Mbps(3),half-100Mbps(4),full-100Mbps(5),half-1Gigabps(6),full-1Gigabps(7)}
	'swL2IGMPCurrentState'   => '1.3.6.1.4.1.171.11.113.1.3.2.7.3.1.10', # (.*)  This object indicates the current IGMP query state. {other(1),querier(2),non-querier(3)}
	'swL2IGMPInfoQueryCount' => '1.3.6.1.4.1.171.11.113.1.3.2.7.4.1.2', # (.*) Количество IGMP пакетов принятых по каждому V-лану
	'swL2LoopDetectPortLoopStatus'=> '1.3.6.1.4.1.171.11.113.1.3.2.21.2.1.1.4' # (.*) Детектор петель на порту {normal(1),loop(2),error(3)}
);

my %dlink17=(                           #   D-Link DMC-1000i v1.16 (enterprise.171.41.1)шасси медиаконвертора 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
#-- mediaconvertors --
	'mcbMCSlotExist'              => '1.3.6.1.4.1.171.41.1.4.2.1.2', # Each Device of Slot Is Exist Or Not {exist(1),nonexist(2),unsupported(3)}
	'mcbMCModelName'              => '1.3.6.1.4.1.171.41.1.4.2.1.3', # Each Media Converter Device Relative Model Name 
	'mcbMCMediaOneType'           => '1.3.6.1.4.1.171.41.1.4.2.1.4', # Media Type Is TP Or Fiber  {tp(1),fiber(2),unsupported(3)}
	'mcbMCMediaTwoType'           => '1.3.6.1.4.1.171.41.1.4.2.1.5', # Media Type Is TP Or Fiber  {tp(1),fiber(2),unsupported(3)}
	'mcbMCMediaOneLinkStatus'     => '1.3.6.1.4.1.171.41.1.4.2.1.6', # Media One Link Status {online(1),offline(2),unsupported(3)}
	'mcbMCMediaTwoLinkStatus'     => '1.3.6.1.4.1.171.41.1.4.2.1.7', # Media Two Link Status {online(1),offline(2),unsupported(3)}
	'mcbMCMediaOneBrokenStatus'   => '1.3.6.1.4.1.171.41.1.4.2.1.27', # Media One Broken Status of the smart Media Converter {unbroken(1),broken(2),unsupported(3)}
	'mcbMCMediaTwoBrokenStatus'   => '1.3.6.1.4.1.171.41.1.4.2.1.28'  # Media Two Broken Status of the smart Media Converter {unbroken(1),broken(2),unsupported(3)}
);

my %dlink18=(                           # D-Link DES-3200-10 Fast Ethernet Switch (enterprise.171.10.113.1.1) 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Power, Fan, Temp --
#	'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
#	'swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
#	'swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
#	'swFanUnitIndex'              => '1.3.6.1.4.1.171.12.11.1.7.1.1', # выдаёт 2 строки для 2-х вентиляторов swFanUnitIndex.1.7 = 1, swFanUnitIndex.1.8 = 1
#	'swFanID'                     => '1.3.6.1.4.1.171.12.11.1.7.1.2', # выдаёт 2 строки ID каждого вентилятора swFanID.1.7 = 7, swFanID.1.8 = 8
#	'swFanStatus'                 => '1.3.6.1.4.1.171.12.11.1.7.1.3', # выдаёт 2 строки состояние каждого вентилятора {other(0),working(1),fail(2),speed-0(3),speed-low(4),speed-middle(5),speed-high(6)}
#	'swTemperatureCurrent'        => '1.3.6.1.4.1.171.12.11.1.8.1.2', # информация с температурного датчика(ов) в градусах
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
#	'agentFirmwareType'           => '1.3.6.1.4.1.171.12.1.1.13', # тип прошивки
	#--Link status--
	'swL2PortInfoLinkStatus' => '1.3.6.1.4.1.171.11.113.1.1.2.2.1.1.4', # (.*)  This object indicates the port link status. {other(1),link-pass(2),link-fail(3)}
	'swL2PortInfoNwayStatus' => '1.3.6.1.4.1.171.11.113.1.1.2.2.1.1.5', # (.*)Режим работы портов {auto(1),half-10Mbps(2),full-10Mbps(3),half-100Mbps(4),full-100Mbps(5),half-1Gigabps(6),full-1Gigabps(7)}
	'swL2IGMPCurrentState'   => '1.3.6.1.4.1.171.11.113.1.1.2.7.3.1.10', # (.*)  This object indicates the current IGMP query state. {other(1),querier(2),non-querier(3)}
	'swL2IGMPInfoQueryCount' => '1.3.6.1.4.1.171.11.113.1.1.2.7.4.1.2', # (.*) Количество IGMP пакетов принятых по каждому V-лану
	'swL2LoopDetectPortLoopStatus'=> '1.3.6.1.4.1.171.11.113.1.1.2.21.2.1.1.4' # (.*) Детектор петель на порту {normal(1),loop(2),error(3)}
);

my %dlink19=(                           # D-Link DES-3200-18 Fast Ethernet Switch (enterprise.171.10.113.1.2) 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Power, Fan, Temp --
#	'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
#	'swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
#	'swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
#	'swFanUnitIndex'              => '1.3.6.1.4.1.171.12.11.1.7.1.1', # выдаёт 2 строки для 2-х вентиляторов swFanUnitIndex.1.7 = 1, swFanUnitIndex.1.8 = 1
#	'swFanID'                     => '1.3.6.1.4.1.171.12.11.1.7.1.2', # выдаёт 2 строки ID каждого вентилятора swFanID.1.7 = 7, swFanID.1.8 = 8
#	'swFanStatus'                 => '1.3.6.1.4.1.171.12.11.1.7.1.3', # выдаёт 2 строки состояние каждого вентилятора {other(0),working(1),fail(2),speed-0(3),speed-low(4),speed-middle(5),speed-high(6)}
#	'swTemperatureCurrent'        => '1.3.6.1.4.1.171.12.11.1.8.1.2', # информация с температурного датчика(ов) в градусах
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
#	'agentFirmwareType'           => '1.3.6.1.4.1.171.12.1.1.13', # тип прошивки
	#--Link status--
	'swL2PortInfoLinkStatus' => '1.3.6.1.4.1.171.11.113.1.2.2.2.1.1.4', # (.*)  This object indicates the port link status. {other(1),link-pass(2),link-fail(3)}
	'swL2PortInfoNwayStatus' => '1.3.6.1.4.1.171.11.113.1.2.2.2.1.1.5', # (.*)Режим работы портов {auto(1),half-10Mbps(2),full-10Mbps(3),half-100Mbps(4),full-100Mbps(5),half-1Gigabps(6),full-1Gigabps(7)}
	'swL2IGMPCurrentState'   => '1.3.6.1.4.1.171.11.113.1.2.2.7.3.1.10', # (.*)  This object indicates the current IGMP query state. {other(1),querier(2),non-querier(3)}
	'swL2IGMPInfoQueryCount' => '1.3.6.1.4.1.171.11.113.1.2.2.7.4.1.2', # (.*) Количество IGMP пакетов принятых по каждому V-лану
	'swL2LoopDetectPortLoopStatus'=> '1.3.6.1.4.1.171.11.113.1.2.2.21.2.1.1.4' # (.*) Детектор петель на порту {normal(1),loop(2),error(3)}
);

my %dlink20=(                           # D-Link DES-3200-18/Cx Fast Ethernet Switch (enterprise.171.10.113.3.1) 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Power, Fan, Temp --
#	'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
#	'swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
#	'swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
#	'swFanUnitIndex'              => '1.3.6.1.4.1.171.12.11.1.7.1.1', # выдаёт 2 строки для 2-х вентиляторов swFanUnitIndex.1.7 = 1, swFanUnitIndex.1.8 = 1
#	'swFanID'                     => '1.3.6.1.4.1.171.12.11.1.7.1.2', # выдаёт 2 строки ID каждого вентилятора swFanID.1.7 = 7, swFanID.1.8 = 8
#	'swFanStatus'                 => '1.3.6.1.4.1.171.12.11.1.7.1.3', # выдаёт 2 строки состояние каждого вентилятора {other(0),working(1),fail(2),speed-0(3),speed-low(4),speed-middle(5),speed-high(6)}
#	'swTemperatureCurrent'        => '1.3.6.1.4.1.171.12.11.1.8.1.2', # информация с температурного датчика(ов) в градусах
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
#	'agentFirmwareType'           => '1.3.6.1.4.1.171.12.1.1.13', # тип прошивки
	#--Link status--
	'swL2PortInfoLinkStatus' => '1.3.6.1.4.1.171.11.113.3.1.2.3.1.1.5', # (.*)  This object indicates the port link status. {other(1),link-pass(2),link-fail(3)}
	'swL2PortInfoNwayStatus' => '1.3.6.1.4.1.171.11.113.3.1.2.3.1.1.6', # (.*) {other(0),empty(1),link-down(2),half-10Mbps(3),full-10Mbps(4),half-100Mbps(5),full-100Mbps(6),half-1Gigabps(7),full-1Gigabps(8),full-10Gigabps(9)}
	'swL2IGMPCurrentState'   => '1.3.6.1.4.1.171.11.113.3.1.2.11.4.1.10', # (.*)  This object indicates the current IGMP query state. {other(1),querier(2),non-querier(3)}
	'swL2IGMPInfoQueryCount' => '1.3.6.1.4.1.171.11.113.3.1.2.11.5.1.2', # (.*) Количество IGMP пакетов принятых по каждому V-лану
	'swL2LoopDetectPortLoopStatus'=> '1.3.6.1.4.1.171.11.113.3.1.2.18.2.1.1.4', # (.*) Детектор петель на порту {normal(1),loop(2),error(3), mone(4)}
);

my %dlink21=(                           # D-Link DES-3200-10/Cx Fast Ethernet Switch (enterprise.171.10.113.2.1) 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics --
	'swEtherCableDiagLinkStatus'  => '1.3.6.1.4.1.171.12.58.1.1.1.3', # (.*) состояние линка-соединения 	
	'swEtherCableDiagPair1Status' => '1.3.6.1.4.1.171.12.58.1.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair2Status' => '1.3.6.1.4.1.171.12.58.1.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair3Status' => '1.3.6.1.4.1.171.12.58.1.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair4Status' => '1.3.6.1.4.1.171.12.58.1.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),unknown(5),count(6),no-cable(7),other(8)} 	
	'swEtherCableDiagPair1Length' => '1.3.6.1.4.1.171.12.58.1.1.1.8', # (.*) примерная длина кабеля 1 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair2Length' => '1.3.6.1.4.1.171.12.58.1.1.1.9', # (.*) примерная длина кабеля 2 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair3Length' => '1.3.6.1.4.1.171.12.58.1.1.1.10', # (.*) примерная длина кабеля 3 пары в метрах (работает при длине не менее 2м) 	
	'swEtherCableDiagPair4Length' => '1.3.6.1.4.1.171.12.58.1.1.1.11', # (.*) примерная длина кабеля 4 пары в метрах (работает при длине не менее 2м) 
	#-- CPU, Port, RAM utilization --
	'agentCPUutilizationIn5sec'   => '1.3.6.1.4.1.171.12.1.1.6.1', # загрузка CPU в % за 5сек интервал
	'agentCPUutilizationIn1min'   => '1.3.6.1.4.1.171.12.1.1.6.2', # загрузка CPU в % за 1мин  интервал
	'agentCPUutilizationIn5min'   => '1.3.6.1.4.1.171.12.1.1.6.3', # загрузка CPU в % за 5мин интервал
	'agentPORTutilizationTX'      => '1.3.6.1.4.1.171.12.1.1.8.1.2', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationRX'      => '1.3.6.1.4.1.171.12.1.1.8.1.3', # (.*) текущая интенсивность отправляемых пакетов по порту
	'agentPORTutilizationUtil'    => '1.3.6.1.4.1.171.12.1.1.8.1.4', # (.*) текущее процентное отношение статистики использования порта
	'agentDRAMutilization'        => '1.3.6.1.4.1.171.12.1.1.9.1.4', # (.*) загрузка оперативной памяти 0-100% 
	#-- Power, Fan, Temp --
#	'swPowerUnitIndex'            => '1.3.6.1.4.1.171.12.11.1.6.1.1', # выдаёт 2 строки с id=1  источников питания  (осн., акк) 
#	'swPowerID'                   => '1.3.6.1.4.1.171.12.11.1.6.1.2', # выдаёт 2 строки соответствие id - swPowerID.1.1 = 1 основной источник, swPowerID.1.2 = 2 аккум.
#	'swPowerStatus'               => '1.3.6.1.4.1.171.12.11.1.6.1.3', # выдаёт 2 строки состояние по каждому источнику {other(0),lowVoltage(1),overCurrent(2),working(3),fail(4),connect(5),disconnect(6)}
#	'swFanUnitIndex'              => '1.3.6.1.4.1.171.12.11.1.7.1.1', # выдаёт 2 строки для 2-х вентиляторов swFanUnitIndex.1.7 = 1, swFanUnitIndex.1.8 = 1
#	'swFanID'                     => '1.3.6.1.4.1.171.12.11.1.7.1.2', # выдаёт 2 строки ID каждого вентилятора swFanID.1.7 = 7, swFanID.1.8 = 8
#	'swFanStatus'                 => '1.3.6.1.4.1.171.12.11.1.7.1.3', # выдаёт 2 строки состояние каждого вентилятора {other(0),working(1),fail(2),speed-0(3),speed-low(4),speed-middle(5),speed-high(6)}
#	'swTemperatureCurrent'        => '1.3.6.1.4.1.171.12.11.1.8.1.2', # информация с температурного датчика(ов) в градусах
	'agentSerialNumber'	      => '1.3.6.1.4.1.171.12.1.1.12', # серийный номер устройства
#	'agentFirmwareType'           => '1.3.6.1.4.1.171.12.1.1.13', # тип прошивки
	#--Link status--
	'swL2PortInfoLinkStatus' => '1.3.6.1.4.1.171.11.113.2.1.2.3.1.1.5', # (.*)  This object indicates the port link status. {other(1),link-pass(2),link-fail(3)}
	'swL2PortInfoNwayStatus' => '1.3.6.1.4.1.171.11.113.2.1.2.	3.1.1.6', # (.*) {other(0),empty(1),link-down(2),half-10Mbps(3),full-10Mbps(4),half-100Mbps(5),full-100Mbps(6),half-1Gigabps(7),full-1Gigabps(8),full-10Gigabps(9)}
	'swL2IGMPCurrentState'   => '1.3.6.1.4.1.171.11.113.2.1.2.11.4.1.10', # (.*)  This object indicates the current IGMP query state. {other(1),querier(2),non-querier(3)}
	'swL2IGMPInfoQueryCount' => '1.3.6.1.4.1.171.11.113.2.1.2.11.5.1.2', # (.*) Количество IGMP пакетов принятых по каждому V-лану
	'swL2LoopDetectPortLoopStatus'=> '1.3.6.1.4.1.171.11.113.2.1.2.18.2.1.1.4', # (.*) Детектор петель на порту {normal(1),loop(2),error(3), mone(4)}
);

my %dlink22=(                           # DGS-1210-28P/ME/A1 (enterprise.171.10.76.30.1)
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics -- DGS-1210-28P/ME/A1 
	'cableDiagLinkStatus'         => '1.3.6.1.4.1.171.10.76.30.1.35.1.1.3', # (.*) состояние линка-соединения {linkdown(0),linkup(1),other(2)} 	
	'cableDiagPair1Status'        => '1.3.6.1.4.1.171.10.76.30.1.35.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)}	
	'cableDiagPair2Status'        => '1.3.6.1.4.1.171.10.76.30.1.35.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)}	
	'cableDiagPair3Status'        => '1.3.6.1.4.1.171.10.76.30.1.35.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)} 	
	'cableDiagPair4Status'        => '1.3.6.1.4.1.171.10.76.30.1.35.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)}	
	'cableDiagPair1Length'        => '1.3.6.1.4.1.171.10.76.30.1.35.1.1.8', # (.*) Cable Diagnostics pair 1 fault distance.
	'cableDiagPair2Length'        => '1.3.6.1.4.1.171.10.76.30.1.35.1.1.9', # (.*) Cable Diagnostics pair 2 fault distance. 
	'cableDiagPair3Length'        => '1.3.6.1.4.1.171.10.76.30.1.35.1.1.10', # (.*) Cable Diagnostics pair 3 fault distance.
	'cableDiagPair4Length'        => '1.3.6.1.4.1.171.10.76.30.1.35.1.1.11', # (.*) Cable Diagnostics pair 4 fault distance.
	#-- CPU, Port, RAM utilization --
	'sysSerialNumber'	      => '1.3.6.1.4.1.171.10.76.30.1.1.30', # серийный номер устройства
	'sysFirmwareVersion'          => '1.3.6.1.4.1.171.10.76.30.1.1.3', # тип прошивки
);

my %dlink23=(                           # DGS-1210-28P/ME/B1 (enterprise.171.10.76.30.2)
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- Cabel diagnostics -- DGS-1210-28P/ME/B1 
	'cableDiagLinkStatus'         => '1.3.6.1.4.1.171.10.76.30.2.35.1.1.3', # (.*) состояние линка-соединения {linkdown(0),linkup(1),other(2)} 	
	'cableDiagPair1Status'        => '1.3.6.1.4.1.171.10.76.30.2.35.1.1.4', # (.*) состояние 1 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)}	
	'cableDiagPair2Status'        => '1.3.6.1.4.1.171.10.76.30.2.35.1.1.5', # (.*) состояние 2 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)}	
	'cableDiagPair3Status'        => '1.3.6.1.4.1.171.10.76.30.2.35.1.1.6', # (.*) состояние 3 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)} 	
	'cableDiagPair4Status'        => '1.3.6.1.4.1.171.10.76.30.2.35.1.1.7', # (.*) состояние 4 пары {ok(0),open(1),short(2),open-short(3),crosstalk(4),mismatch(5),linedriver(6),unknown(7),count(8),no-cable(9),other(10)}	
	'cableDiagPair1Length'        => '1.3.6.1.4.1.171.10.76.30.2.35.1.1.8', # (.*) Cable Diagnostics pair 1 fault distance.
	'cableDiagPair2Length'        => '1.3.6.1.4.1.171.10.76.30.2.35.1.1.9', # (.*) Cable Diagnostics pair 2 fault distance. 
	'cableDiagPair3Length'        => '1.3.6.1.4.1.171.10.76.30.2.35.1.1.10', # (.*) Cable Diagnostics pair 3 fault distance.
	'cableDiagPair4Length'        => '1.3.6.1.4.1.171.10.76.30.2.35.1.1.11', # (.*) Cable Diagnostics pair 4 fault distance.
	#-- CPU, Port, RAM utilization --
	'sysSerialNumber'	      => '1.3.6.1.4.1.171.10.76.30.2.1.30', # серийный номер устройства
	'sysFirmwareVersion'          => '1.3.6.1.4.1.171.10.76.30.2.1.3', # тип прошивки
);
my %snr0=(                           # SNR-S2970-12X Series Software, Version 2.1.1B Build 31318 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
#	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
#	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам

);

my %snr1=(                           # SNR-S2990-16X Device, Compiled Oct 11 10:19:13 2018 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- enterprise ---
	#-- sysSlot --
	'sysHardwareVersion'          => '1.3.6.1.4.1.40418.7.100.1.2', # The version information of switch hardware 
	'sysSoftwareVersion'          => '1.3.6.1.4.1.40418.7.100.1.3', # The version information of switch software 
	'sysCPUFiveMinuteIdle'        => '1.3.6.1.4.1.40418.7.100.1.8.1.5', # Last  5 minute CPU IDLE
	'sysCPUIdle'                  => '1.3.6.1.4.1.40418.7.100.1.8.1.6', #  From  running  CPU IDLE
	'sysTemperature'              => '1.3.6.1.4.1.40418.7.100.1.8.1.10', # The Temperature of this slot.80 is 80centigrade
	#-- switchinfo --
	'sysCpuUsage'                 => '1.3.6.1.4.1.40418.7.100.1.8.1.11', # The CPU usage of this slot.
	'switchCPUFiveMinuteIdle'     => '1.3.6.1.4.1.40418.7.100.1.11.4', # Last  5 minute CPU IDLE
	'switchCPUIdle'               => '1.3.6.1.4.1.40418.7.100.1.11.5', # From  running  CPU IDLE
	'switchTemperature'           => '1.3.6.1.4.1.40418.7.100.1.11.9', # The Temperature of switch.80 is 80centigrade
	'switchCpuUsage'              => '1.3.6.1.4.1.40418.7.100.1.11.10', # The CPU usage of switch.
	'switchMemoryUsage'           => '1.3.6.1.4.1.40418.7.100.1.11.11', #  The memory usage of switch.
	'sysFanInserted'              => '1.3.6.1.4.1.40418.7.100.1.12.1.2', # Whether the slot fan is installed or not. {sysFanNotInstalled(0),sysFanInstalled(1)}
	'sysFanStatus'                => '1.3.6.1.4.1.40418.7.100.1.12.1.3', # Whether the slot fan is normal or abnormal.
	'showSwitchStartUpStatus'     => '1.3.6.1.4.1.40418.7.100.1.16', #  this object identifies start up status of switch and AM of chassis.{idle (1),success (2),fail(3),in-progress(4)}
	'transceiverSn'               => '1.3.6.1.4.1.40418.7.100.3.2.1.28', # serial num of transceiver.if port is not a transceiver,value is null.
	'vlanFCVlanID'                => '1.3.6.1.4.1.40418.7.100.5.4.1.1', # This value is vlan's ID.

);

my %snr2=(                           # SNR-S2990G-24FX Device, Compiled Jan 09 17:26:07 2017 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- enterprise ---
	#-- sysSlot --
	'sysHardwareVersion'          => '1.3.6.1.4.1.40418.7.100.1.2', # The version information of switch hardware 
	'sysSoftwareVersion'          => '1.3.6.1.4.1.40418.7.100.1.3', # The version information of switch software 
#	'sysCPUFiveMinuteIdle'        => '1.3.6.1.4.1.40418.7.100.1.8.1.5', # Last  5 minute CPU IDLE
#	'sysCPUIdle'                  => '1.3.6.1.4.1.40418.7.100.1.8.1.6', #  From  running  CPU IDLE
#	'sysTemperature'              => '1.3.6.1.4.1.40418.7.100.1.8.1.10', # The Temperature of this slot.80 is 80centigrade
	#-- switchinfo --
#	'sysCpuUsage'                 => '1.3.6.1.4.1.40418.7.100.1.8.1.11', # The CPU usage of this slot.
	'switchCPUFiveMinuteIdle'     => '1.3.6.1.4.1.40418.7.100.1.11.4', # Last  5 minute CPU IDLE
	'switchCPUIdle'               => '1.3.6.1.4.1.40418.7.100.1.11.5', # From  running  CPU IDLE
	'switchTemperature'           => '1.3.6.1.4.1.40418.7.100.1.11.9', # The Temperature of switch.80 is 80centigrade
	'switchCpuUsage'              => '1.3.6.1.4.1.40418.7.100.1.11.10', # The CPU usage of switch.
	'switchMemoryUsage'           => '1.3.6.1.4.1.40418.7.100.1.11.11', #  The memory usage of switch.
	'sysFanInserted'              => '1.3.6.1.4.1.40418.7.100.1.12.1.2', # Whether the slot fan is installed or not. {sysFanNotInstalled(0),sysFanInstalled(1)}
	'sysFanStatus'                => '1.3.6.1.4.1.40418.7.100.1.12.1.3', # Whether the slot fan is normal or abnormal.
	'showSwitchStartUpStatus'     => '1.3.6.1.4.1.40418.7.100.1.16', #  this object identifies start up status of switch and AM of chassis.{idle (1),success (2),fail(3),in-progress(4)}
#	'transceiverSn'               => '1.3.6.1.4.1.40418.7.100.3.2.1.28', # serial num of transceiver.if port is not a transceiver,value is null.
	'vlanFCVlanID'                => '1.3.6.1.4.1.40418.7.100.5.4.1.1', # This value is vlan's ID.

);

my %snr3=(                           # SNR-S3650G-48S Device, Compiled May 16 20:59:08
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- enterprise ---
	#-- sysSlot --
	'sysHardwareVersion'          => '1.3.6.1.4.1.40418.7.100.1.2', # The version information of switch hardware 
	'sysSoftwareVersion'          => '1.3.6.1.4.1.40418.7.100.1.3', # The version information of switch software 
#	'sysCPUFiveMinuteIdle'        => '1.3.6.1.4.1.40418.7.100.1.8.1.5', # Last  5 minute CPU IDLE
#	'sysCPUIdle'                  => '1.3.6.1.4.1.40418.7.100.1.8.1.6', #  From  running  CPU IDLE
#	'sysTemperature'              => '1.3.6.1.4.1.40418.7.100.1.8.1.10', # The Temperature of this slot.80 is 80centigrade
	#-- switchinfo --
#	'sysCpuUsage'                 => '1.3.6.1.4.1.40418.7.100.1.8.1.11', # The CPU usage of this slot.
	'switchCPUFiveMinuteIdle'     => '1.3.6.1.4.1.40418.7.100.1.11.4', # Last  5 minute CPU IDLE
	'switchCPUIdle'               => '1.3.6.1.4.1.40418.7.100.1.11.5', # From  running  CPU IDLE
	'switchTemperature'           => '1.3.6.1.4.1.40418.7.100.1.11.9', # The Temperature of switch.80 is 80centigrade
	'switchCpuUsage'              => '1.3.6.1.4.1.40418.7.100.1.11.10', # The CPU usage of switch.
#	'switchMemoryUsage'           => '1.3.6.1.4.1.40418.7.100.1.11.11', #  The memory usage of switch.
	'sysFanInserted'              => '1.3.6.1.4.1.40418.7.100.1.12.1.2', # Whether the slot fan is installed or not. {sysFanNotInstalled(0),sysFanInstalled(1)}
	'sysFanStatus'                => '1.3.6.1.4.1.40418.7.100.1.12.1.3', # Whether the slot fan is normal or abnormal.
	'showSwitchStartUpStatus'     => '1.3.6.1.4.1.40418.7.100.1.16', #  this object identifies start up status of switch and AM of chassis.{idle (1),success (2),fail(3),in-progress(4)}
#	'transceiverSn'               => '1.3.6.1.4.1.40418.7.100.3.2.1.28', # serial num of transceiver.if port is not a transceiver,value is null.
#	'vlanFCVlanID'                => '1.3.6.1.4.1.40418.7.100.5.4.1.1', # This value is vlan's ID.
);

my %vermax1=(                           # V1.X 2012/06/16 optical transmitter
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
#	'ifAdminStatus'               => '1.3.6.1.2.1.2.2.1.7', # массив значений по каждому сетевому интерфейсу
#	'ifOperStatus'                => '1.3.6.1.2.1.2.2.1.8', # массив значений по каждому сетевому интерфейсу
#	'ifInUcastPkts'               => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу
#	'ifInNUcastPkts'              => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу
#	'ifInErrors'                  => '1.3.6.1.2.1.2.2.1.14', # массив значений по каждому сетевому интерфейсу
#	'ifOutUcastPkts'              => '1.3.6.1.2.1.2.2.1.11', # массив значений по каждому сетевому интерфейсу точка-точка
#	'ifOutNUcastPkts'             => '1.3.6.1.2.1.2.2.1.12', # массив значений по каждому сетевому интерфейсу широковещат+мультикаст
#	'ifOutErrors'                 => '1.3.6.1.2.1.2.2.1.20', # массив значений по каждому сетевому интерфейсу
#	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
#	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
#	'ifInMulticastPkts'           => '1.3.6.1.2.1.31.1.1.1.2', # (.*) количество мультикаст пакетов по  портам	
#	'ifInBroadcastPkts'           => '1.3.6.1.2.1.31.1.1.1.3', # (.*) количество широковещат  пакетов по  портам	
#	'ifOutMulticastPkts'          => '1.3.6.1.2.1.31.1.1.1.4', # (.*) количество мультикаст пакетов по всем портам	
#	'ifOutBroadcastPkts'          => '1.3.6.1.2.1.31.1.1.1.5', # (.*) количество широковещат пакетов по всем портам
	#-- enterprise ---
	'otxInputRFLevel'             => '1.3.6.1.4.1.17409.1.7.3.1.12', # уровень входного сигнала (выдаёт 930)
	'otxRfGain'                   => '1.3.6.1.4.1.17409.1.7.3.1.13', # значение усиления сигнала (выдаёт 0)
	'otxLaserCurrent'             => '1.3.6.1.4.1.17409.1.7.3.1.14', # ток лазера (выдает 84 )
	'otxLaserOutputPower'         => '1.3.6.1.4.1.17409.1.7.3.1.15', # выходная мощность лазера (выдаёт 3)
	'otxLaserTemperature'         => '1.3.6.1.4.1.17409.1.7.3.1.16', # температура лазера (выдаёт 2540)
	'otxLaserTecCurrent'          => '1.3.6.1.4.1.17409.1.7.3.1.17', # нет описания (выдаёт -740 )
	'otxFansNumber'               => '1.3.6.1.4.1.17409.1.7.4', # количество вентиляторов (1)
	'otxNumberDCPowerSupply'      => '1.3.6.1.4.1.17409.1.7.6', # количество источников питания (2)
	'otxDCPowerSupplyMode'        => '1.3.6.1.4.1.17409.1.7.7', # Режим запитки (выдаёт 2)   {loadsharing(1), switchedRedundant(2), aloneSupply(3)}
	'otxDCPowerVoltage'           => '1.3.6.1.4.1.17409.1.7.8.1.2', # значение напряжения по каждой питающей линии (239 119 -115 49 -51 значения без запятых)
	'otxDCPowerCurrent'           => '1.3.6.1.4.1.17409.1.7.8.1.3', # значения токов потребления по каждому напряжению (выдаёт нули)
	'otxDCPowerName'              => '1.3.6.1.4.1.17409.1.7.8.1.4', # питающие шины. 
);

my %vermax2=(                           # V3.X-V4.X  2013/10/11 (enterprise.17409.1.10) optical receiver
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	#-- enterprise ---
	'fnReturnLaserCurrent'        => '1.3.6.1.4.1.17409.1.10.3.1.2', # (0)
	'fnReturnLaserTemp'           => '1.3.6.1.4.1.17409.1.10.3.1.3', # (0) 
	'fnReturnLaserType'           => '1.3.6.1.4.1.17409.1.10.3.1.5', # (isolated FP)
	'fnReturnLaserWavelength'     => '1.3.6.1.4.1.17409.1.10.3.1.6', # (1310)
	'fnReverseOpticalPower'       => '1.3.6.1.4.1.17409.1.10.3.1.7', # (-999)
	'fnReturnLaserRFActive'       => '1.3.6.1.4.1.17409.1.10.3.1.8', # (1)
	'fnNumberOpticalReceiver'     => '1.3.6.1.4.1.17409.1.10.4', # (1)
	'fnOpticalAmpPresent'         => '1.3.6.1.4.1.17409.1.10.6', #	 (1) {no(1), yes(2) }
	'fnNumberRFActives'           => '1.3.6.1.4.1.17409.1.10.7', # (1)
	'fnRFPortControlType'         => '1.3.6.1.4.1.17409.1.10.11.1.2', # (1) { alc(1), asc(2), agc(3), none(4) }
	'fnRFPortOutputRFLevel'       => '1.3.6.1.4.1.17409.1.10.11.1.4', # (102)
	'fnRFPortRFActive'            => '1.3.6.1.4.1.17409.1.10.11.1.5', # (1)
	'fnRFPortName'                => '1.3.6.1.4.1.17409.1.10.11.1.6', # (RFPORT1)
	'fnRFPortPowerFeedStatus'     => '1.3.6.1.4.1.17409.1.10.11.1.8', # (2)  {on(1),off(2) }
	'fnNumberDCPowerSupply'       => '1.3.6.1.4.1.17409.1.10.17', # (2) количество источников питания
	'fnDCPowerSupplyMode'         => '1.3.6.1.4.1.17409.1.10.18', # (1) {loadsharing(1), switchedRedundant(2) }
	'fnDCPowerVoltage'            => '1.3.6.1.4.1.17409.1.10.19.1.2', # (82  235) напряжения источников питания 8.2в 23.5в
	'fnDCPowerCurrent'            => '1.3.6.1.4.1.17409.1.10.19.1.3', # (null) потребление тока
	'fnDCPowerName'               => '1.3.6.1.4.1.17409.1.10.19.1.4', # (+8v +24v)
	#-- optical receiver--
	'fnOpticalReceiverPower'      => '1.3.6.1.4.1.17409.1.10.5.1.2',  # добавил по просьбе Карташова
);

my %vermax3=(                           # EDFA SNMP AGENT System optical amplifier (enterprise.17409.1.11)
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	#-- enterprise ---
	'oaOutputOpticalPower'        => '1.3.6.1.4.1.17409.1.11.2', # (208) выходная мощность сигнала (можно задать)
	'oaInputOpticalPower'         => '1.3.6.1.4.1.17409.1.11.3', # (-37) мощность входного сигнала
	'oaPumpBIAS'                  => '1.3.6.1.4.1.17409.1.11.4.1.2', # (556)
	'oaPumpTEC'                   => '1.3.6.1.4.1.17409.1.11.4.1.3', # (0)
	'oaPumpTemp'                  => '1.3.6.1.4.1.17409.1.11.4.1.4', # (252)
	'oaNumberDCPowerSupply'       => '1.3.6.1.4.1.17409.1.11.5', # (2)
	'oaDCPowerSupplyMode'         => '1.3.6.1.4.1.17409.1.11.6', # (2)   {loadsharing(1), switchedRedundant(2), aloneSupply(3) }
	'oaDCPowerVoltage'            => '1.3.6.1.4.1.17409.1.11.7.1.2', # (48 -50) питающее напряжение, значения бе запеятых (*0.1)
	'oaDCPowerCurrent'            => '1.3.6.1.4.1.17409.1.11.7.1.3', # (0 0) потребляемый ток
	'oaDCPowerName'               => '.1.3.6.1.4.1.17409.1.11.7.1.4', # (DC +5V DC -5V)

);

my %vermax4=(                           # EDFA System VERMAX-MLP-16x18  (enterprise.5591.1.11)
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	'entPhysicalHardwareRev'      => '1.3.6.1.2.1.47.1.1.1.1.8', # hardware revision
	'entPhysicalFirmwareRev'      => '1.3.6.1.2.1.47.1.1.1.1.9', # firmware revision
	'entPhysicalSoftwareRev'      => '1.3.6.1.2.1.47.1.1.1.1.10', # software revision
	'entPhysicalSerialNum'        => '1.3.6.1.2.1.47.1.1.1.1.11', # строка с серийником
	'entPhysicalModelName'        => '1.3.6.1.2.1.47.1.1.1.1.13', # название модели
	'entPhysicalDescr'            => '1.3.6.1.2.1.47.1.1.1.1.2' , # (.*) 1 строка-модель устройства далее описание каждого порта 100мбит 10000 fiber copper
	#-- enterprise ---
	'heOpAmpInputPower'           => '1.3.6.1.4.1.5591.1.11.1.3.1.1.2.1.2', # (62) Optical input power. Tenth dBm (-1)
	'heOpAmpLaserTemp'            => '1.3.6.1.4.1.5591.1.11.1.3.1.1.3.1.2', # (249 250) Temperature of the amplifier laser (24.9 24.0 grad)
	'heOpAmpLaserOutputPower'     => '1.3.6.1.4.1.5591.1.11.1.3.1.1.3.1.4', # (160 100) The laser output power. Tenth dBm (-1)
	'heOpAmpLaserType'            => '1.3.6.1.4.1.5591.1.11.1.3.1.1.3.1.6', # (DFB DFB)  Laser type.
	
);

my %erd_fmv=(                           # SNR-ERD  Fmv_6.2 (enterprise.40418.2.2) snmp v1 only!
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
	#-- enterprise ---
	'temperatureSensor'           => '1.3.6.1.4.1.40418.2.2.4.1', # (30) температура в градусах
	'voltageSensorContact10'      => '1.3.6.1.4.1.40418.2.2.4.2', # (1420) Напряжение на сенсоре 14.2V	
);

my %tesla_mini=(                           #   mini tesla tp-ats-06a02b-1u (enterprise.30966) 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
);


my %pbi=(                           # Made by PBI  Model:DMM-1701IM  SW Version:17IM002B (enterprise.38295.15.0.0) 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
);

my %eltex=(                           # ELTEX LTE-2X (enterprise.35265.1.21.202.2)
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
);

my %emr=(                           # Enhanced Multimedia Router (enterprise.32285.2.2.1)
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
);

my %net_agent=(                           # UPS NET Agent II (enterprise.935) 
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
);

my %routeros=(                          # RouterOS RB1100AHx2 (enterprise.14988.1)
	'sysDescr'                    => '1.3.6.1.2.1.1.1',   # название модели свитча
	'sysObjectID'                 => '1.3.6.1.2.1.1.2',   # VendorID
	'sysUpTime'                   => '1.3.6.1.2.1.1.3',   # время работы после последней перезагрузки
	'sysName'                     => '1.3.6.1.2.1.1.5',   # присвоенное пользователем имя
);
my %list_vendor_oids = (
			'1.3.6.1.4.1.171.10.61.2' => \%dlink1, # DES-2108     V3.00.27    
			'1.3.6.1.4.1.171.10.113.8.1' => \%dlink2, # DES-3200-28P/C1 Fast Ethernet Switch 
			'1.3.6.1.4.1.171.10.113.9.1' => \%dlink3, # DES-3200-52/C1 Fast Ethernet Switch  
			'1.3.6.1.4.1.171.10.64.1' => \%dlink4, # DES-3526 Fast-Ethernet Switch           
			'1.3.6.1.4.1.171.10.76.28.2' => \%dlink5, # DGS-1210-28/ME/B1
			'1.3.6.1.4.1.171.10.76.29.1' => \%dlink6, # DGS-1210-52/ME/A1
			'1.3.6.1.4.1.171.10.76.29.2' => \%dlink7, # DGS-1210-52/ME/B1
			'1.3.6.1.4.1.171.10.117.1.3' => \%dlink8, # DGS-3120-24SC Gigabit Ethernet Switch
			'1.3.6.1.4.1.171.10.117.1.1' => \%dlink9, # DGS-3120-24TC Gigabit Ethernet Switch
			'1.3.6.1.4.1.171.10.119.2' => \%dlink10, # DGS-3420-28SC Gigabit Ethernet Switch
			'1.3.6.1.4.1.171.10.70.8' => \%dlink11, # DGS-3627G Gigabit Ethernet Switch
			'1.3.6.1.4.1.171.10.63.3' => \%dlink12, # D-Link DES-3026 Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.63.6' => \%dlink13, # D-Link DES-3028 Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.63.11' => \%dlink14, # D-Link DES-3028G Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.63.7' => \%dlink15, # D-Link DES-3028P Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.113.1.3' => \%dlink16, # D-Link DES-3200-28 Fast Ethernet Switch
			'1.3.6.1.4.1.171.41.1' => \%dlink17, # D-Link DMC-1000i v1.16
			'1.3.6.1.4.1.171.10.113.1.1' => \%dlink18, # D-Link DES-3200-10 Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.113.1.2' => \%dlink19, # D-Link DES-3200-18 Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.113.3.1' => \%dlink20, # D-Link DES-3200-18/Cx Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.113.2.1' => \%dlink21, # D-Link DES-3200-10/Cx Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.76.30.1' => \%dlink22, # D-Link DGS-1210-28P/ME/A1 Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.76.30.2' => \%dlink23, # D-Link DGS-1210-28P/ME/B1 Fast Ethernet Switch
			'1.3.6.1.4.1.17409.1.11' => \%vermax3, # EDFA SNMP AGENT System
			'1.3.6.1.4.1.5591.1.11' => \%vermax4, # EDFA System
			'1.3.6.1.4.1.35265.1.21.202.2' => \%eltex, # ELTEX LTE-2X        
			'1.3.6.1.4.1.32285.2.2.1' => \%emr, # Enhanced Multimedia Router 
			'1.3.6.1.4.1.40418.2.2' => \%erd_fmv, # Fmv_6.X
			'1.3.6.1.4.1.38295.15.0.0' => \%pbi, # Made by PBI  Model:DMM-1701IM  SW Version:17IM002B 
			'1.3.6.1.4.1.30966' => \%tesla_mini, # mini tesla tp-ats-06a02b-1u 
			'1.3.6.1.4.1.40418.1.283.0' => \%snr0, # SNR-S2970-12X Series Software, Version 2.1.1B Build 31318
			'1.3.6.1.4.1.935' => \%net_agent, # UPS NET Agent II  Phoenixtec Power Co., Ltd. 
			'1.3.6.1.4.1.144988.1' => \%routeros, # RouterOS RB1100AHx2 
			'1.3.6.1.4.1.40418.7.34' => \%snr1, # SNR-S2990-16X Device, Compiled Oct 11 10:19:13 2018
			'1.3.6.1.4.1.40418.7.234' => \%snr2, # SNR-S2990G-24FX Device, Compiled Jan 09 17:26:07 2017
			'1.3.6.1.4.1.40418.7.12' => \%snr3, # SNR-S3650G-48S Device, Compiled May 16 20:59:08
			'1.3.6.1.4.1.17409.1.7' => \%vermax1, # V1.X 2012/06/16
			'1.3.6.1.4.1.17409.1.10' => \%vermax2, # V3.X-V4.X 2013/10/11
);

my %undefined_vendor_oids; # сюда складываются неопознанные системой vendor oids

my %accepted_vendor_oids = ( # oids соответствующие устройствам участвующим в сборе данных
			'1.3.6.1.4.1.171.10.61.2' => \%dlink1, # DES-2108     V3.00.27    
			'1.3.6.1.4.1.171.10.113.8.1' =>\%dlink2, # DES-3200-28P/C1 Fast Ethernet Switch 
			'1.3.6.1.4.1.171.10.113.9.1' => \%dlink3, # DES-3200-52/C1 Fast Ethernet Switch  
			'1.3.6.1.4.1.171.10.64.1' => \%dlink4, # DES-3526 Fast-Ethernet Switch           
			'1.3.6.1.4.1.171.10.76.28.2' => \%dlink5, # DGS-1210-28/ME/B1         
			'1.3.6.1.4.1.171.10.76.29.1' => \%dlink6, # DGS-1210-52/ME/A1
			'1.3.6.1.4.1.171.10.76.29.2' => \%dlink7, # DGS-1210-52/ME/B1
			'1.3.6.1.4.1.171.10.117.1.3' => \%dlink8, # DGS-3120-24SC Gigabit Ethernet Switch
			'1.3.6.1.4.1.171.10.117.1.1' => \%dlink9, # DGS-3120-24TC Gigabit Ethernet Switch
			'1.3.6.1.4.1.171.10.119.2' => \%dlink10, # DGS-3420-28SC Gigabit Ethernet Switch
			'1.3.6.1.4.1.171.10.70.8' => \%dlink11, # DGS-3627G Gigabit Ethernet Switch
			'1.3.6.1.4.1.171.10.63.3' => \%dlink12, # D-Link DES-3026 Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.63.6' => \%dlink13, # D-Link DES-3028 Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.63.11' => \%dlink14, # D-Link DES-3028G Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.63.7' => \%dlink15, # D-Link DES-3028P Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.113.1.3' => \%dlink16, # D-Link DES-3200-28 Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.113.1.1' => \%dlink18, # D-Link DES-3200-10 Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.113.1.2' => \%dlink19, # D-Link DES-3200-18 Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.113.3.1' => \%dlink20, # D-Link DES-3200-18/Cx Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.113.2.1' => \%dlink21, # D-Link DES-3200-10/Cx Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.76.30.1' => \%dlink22, # D-Link DGS-1210-28P/ME/A1 Fast Ethernet Switch
			'1.3.6.1.4.1.171.10.76.30.2' => \%dlink23, # D-Link DGS-1210-28P/ME/B1 Fast Ethernet Switch
			'1.3.6.1.4.1.17409.1.11' => \%vermax3, # EDFA SNMP AGENT System
			'1.3.6.1.4.1.5591.1.11' => \%vermax4, # EDFA System
			'1.3.6.1.4.1.40418.1.283.0' => \%snr0, # SNR-S2970-12X Series Software, Version 2.1.1B Build 31318
			'1.3.6.1.4.1.40418.7.34' => \%snr1, # SNR-S2990-16X Device, Compiled Oct 11 10:19:13 2018
			'1.3.6.1.4.1.40418.7.234' => \%snr2, # SNR-S2990G-24FX Device, Compiled Jan 09 17:26:07 2017
			'1.3.6.1.4.1.40418.7.12' => \%snr3, # SNR-S3650G-48S Device, Compiled May 16 20:59:08
			'1.3.6.1.4.1.17409.1.7' => \%vermax1, # V1.X 2012/06/16
			'1.3.6.1.4.1.17409.1.10' => \%vermax2, # V3.X-V4.X 2013/10/11
);

my %blocked_vendor_oids = (   # устройства не участвующие в сборе данных
			
			'1.3.6.1.4.1.171.41.1' => \%dlink17, # D-Link DMC-1000i v1.16
			'1.3.6.1.4.1.35265.1.21.202.2' => \%eltex, # ELTEX LTE-2X        
			'1.3.6.1.4.1.32285.2.2.1' => \%emr, # Enhanced Multimedia Router 
			'1.3.6.1.4.1.40418.2.2' => \%erd_fmv, # Fmv_6.X
			'1.3.6.1.4.1.38295.15.0.0' => \%pbi, # Made by PBI  Model:DMM-1701IM  SW Version:17IM002B 
			'1.3.6.1.4.1.30966' => \%tesla_mini, # mini tesla tp-ats-06a02b-1u 
			'1.3.6.1.4.1.935' => \%net_agent, # UPS NET Agent II  Phoenixtec Power Co., Ltd. 
			'1.3.6.1.4.1.144988.1' => \%routeros, # RouterOS RB1100AHx2 
	);


#--------------- синхронизатор - time management--------------
sub time_mngmt {
	my $period_t = 300; # период временных меток по умолчанию 300 сек (5 мин)
        my $current_time = time();
        my $time_mark = (int($current_time/$period_t))*$period_t; # пройденная временная метка
        my $new_time_mark = $time_mark + $period_t; # следующая временная метка
        my $delay_time = $new_time_mark - $current_time; # время до старта
return ($time_mark, $new_time_mark, $delay_time);
}

#----- процедура получения информации с одного IP согласно заданных OIDS -----
sub get_ip_data {
	my ($self, $ip, $accepted_vendor_ip)=@_;
	my %data_hsh=(); # хэш для всех данных
	my %merged_hsh=();
	my %main_data_hsh=();
		#unless (defined $ip) {return undef;} 
		#unless (defined $accepted_vendor_ip) {return undef;}
		my $vendor_oid = $accepted_vendor_ip->{$ip};
		my $model_oids = $accepted_vendor_oids{$vendor_oid}; # ссылка на  хэш с оидами модели свича
		#delete $model_oids -> {'sysObjectID'}; # убираем из выборки vendor ID, так как она у нас уже есть
				my $num_oids = scalar keys %{$model_oids};
				my @oids_descr = keys %{$model_oids}; # получаем массив OIDs для нашего адреса
				my $next;
			$next = sub {
			return if (!@oids_descr);
			my $descr = pop (@oids_descr);
#print "$descr\n";
			my $oid = $model_oids -> {$descr};
			my ($session, $error) = Net::SNMP->session(
					-hostname      => $ip,
					-community     => $community,
					-translate     => [-octetstring => 0],
					-version       => 'snmpv2c',
					-timeout       => 2,
				#	-nonblocking   => 1,
				     ); 
			if (!defined $session) {
			printf "ERROR: %s.\n", $error; 
			exit 1}
			my $result = $session->get_table(
					-baseoid => $oid,
					-maxrepetitions => 5,
						);
			if (!defined $result->{$oid}){ my %t_hsh=();
							while (my ($boid,$res_oid) = each %$result) {
								if ($boid =~/\.(\d+)$/){$t_hsh{$1}=$res_oid}} #отрезаем лишнее
							$main_data_hsh{$ip}{$descr} = \%t_hsh}; # здесь можно вкл-откл base_oid или descr
		#print "+++++++++++++++++++++++\n";
				
			$session->close();
			$next->();
				};
		$next->();
undef $ip;
undef $accepted_vendor_ip;
return (\%main_data_hsh);
}


#----- процедура получения vendor_id и не отвечающих по snmp свитчам -----
sub get_vendor_id {    

my ( $self, @node_ipm ) = @_; 
	my @mass_ip_snmp_undef;
       my @tt;
      my %th_hash;
my $sysObjectID = '1.3.6.1.2.1.1.2.0'; # вендор ID
push (my @oids, $sysObjectID) ;
 while (@node_ipm){
               my $ip_=pop @node_ipm;
               unless ($ip_) {last};
               Net::SNMP->session(
                               -hostname      => $ip_,
                               -community     => $community,
                              -translate     => [-octetstring => 0],
                             -version       => 'snmpv1',
                       #       -timeout       => 1,
                               -nonblocking => 1,
                            )->get_request(
                               -varbindlist => \@oids,
                               -delay => 3,
                               -callback => sub {
                       my $snmp_q=shift;
                       my %result;
       if (defined $snmp_q->var_bind_list()){
        $result{$sysObjectID} = $snmp_q->var_bind_list()->{$sysObjectID}; }
               my $rr;
               my @vv=values %result;
               if (@vv==0){@tt[0]='timeout'} else {@tt=values %result}; 
               if ($tt[0] eq 'timeout') {$rr='undefined'; 
					push (@mass_ip_snmp_undef, $ip_)}  # отсеиваем зависшие по SNMP
                       else {$rr = $tt[0]};
	      if ($rr ne 'undefined'){$th_hash{$ip_}=$rr};
       print" thread ip = $ip_ model=$rr; \n";

});
}
snmp_dispatcher(); 
	return (\%th_hash, \@mass_ip_snmp_undef );
}

#----- пинговалка адресов -----
sub ping_ipm {       #  пингуем массив с адресами, на выходе два массива: @ping_ok, @ping_not_ok 
my ($self, $node_ipm) = @_;

my @node_ipm_pinged;
my @node_ipm_notpinged;
	my $asyncping=new AsyncPing(timeout=>3,try=>2); # tested 3-2
	my $result=$asyncping->ping($node_ipm);
#print Dumper $result;
		my $key;
		my $value;
		while ( ($key, $value) = each %$result ) {
			if ($value){ push(@node_ipm_pinged, $key) }
			else { push(@node_ipm_notpinged, $key) };
#print "$key => $value\n";
}
return (\@node_ipm_pinged, \@node_ipm_notpinged);

}

#----- разделяем адреса на 3 списка: известный вендор, заблокированный вендор, неизвестный  --------
sub sort_vendor_id {

my ($self,$node_ipm_pinged_ipm) = @_;
	my %accepted_vendor;
	my %blocked_vendor;
	my %new_vendor;
	
	while (my ($key, $value) = each(%$node_ipm_pinged_ipm)) {
		if ( exists  $blocked_vendor_oids{$value})
			{ $blocked_vendor{$key}=$value; }
		elsif (exists $accepted_vendor_oids{$value})
			{ $accepted_vendor{$key}=$value; }
		else { $new_vendor{$key} = $value; }
		}
return (\%accepted_vendor, \%blocked_vendor, \%new_vendor ); # хэш ip=>vendor_oid	
}

1;

