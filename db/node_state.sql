ATTACH TABLE node_state
(
    `time` DateTime, 
    `node` String, 
    `state` String
)
ENGINE = MergeTree
PARTITION BY toYYYYMM(time)
ORDER BY time
TTL time + toIntervalMonth(3)
SETTINGS index_granularity = 8192
