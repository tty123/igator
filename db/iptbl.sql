ATTACH TABLE iptbl
(
    `id` String, 
    `ips` String, 
    `up_date` Date
)
ENGINE = ReplacingMergeTree(up_date, id, 8192)
