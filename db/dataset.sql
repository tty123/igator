ATTACH TABLE dataset
(
    `time` DateTime, 
    `node` String, 
    `ip` String, 
    `data` String, 
    `condition` String
)
ENGINE = MergeTree
PARTITION BY toYYYYMM(time)
ORDER BY (time, node, ip)
TTL time + toIntervalMonth(3)
SETTINGS index_granularity = 8192
